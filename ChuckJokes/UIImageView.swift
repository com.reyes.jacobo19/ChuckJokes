//
//  UIImageView.swift
//  ChuckJokes
//
//  Created by Jacob Reyes on 4/11/22.
//

import Foundation
import UIKit

extension UIImageView {
    
    func load(url: URL, completion: @escaping (Bool?) -> Void) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                        completion(true)
                    }
                }
            }
            completion(false)
        }
    }
}
