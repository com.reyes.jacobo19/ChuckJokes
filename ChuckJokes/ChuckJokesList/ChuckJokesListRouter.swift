//
//  ChuckJokesListRouter.swift
//  ChuckJokes
//
//  Created by Jacob Reyes on 4/11/22.
//  
//

import Foundation
import UIKit

class ChuckJokesListRouter: ChuckJokesListRouterProtocol {

    class func createChuckJokesListModule() -> UIViewController {
        let view = mainStoryboard.instantiateViewController(withIdentifier: "chuckJokesListView")
        if let viewController = view as? ChuckJokesListView {
            let presenter: ChuckJokesListPresenterProtocol & ChuckJokesListInteractorOutputProtocol = ChuckJokesListPresenter()
            let interactor: ChuckJokesListInteractorInputProtocol & ChuckJokesListRemoteDataManagerOutputProtocol = ChuckJokesListInteractor()
//            let localDataManager: ChuckJokesListLocalDataManagerInputProtocol = ChuckJokesListLocalDataManager()
            let remoteDataManager: ChuckJokesListRemoteDataManagerInputProtocol = ChuckJokesListRemoteDataManager()
            let router: ChuckJokesListRouterProtocol = ChuckJokesListRouter()
            
            viewController.presenter = presenter
            presenter.view = viewController
            presenter.router = router
            presenter.interactor = interactor
            interactor.presenter = presenter
//            interactor.localDatamanager = localDataManager
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return view
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "ChuckJokesList", bundle: Bundle.main)
    }
    
}
