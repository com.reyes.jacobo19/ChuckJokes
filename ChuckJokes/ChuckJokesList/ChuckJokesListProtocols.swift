//
//  ChuckJokesListProtocols.swift
//  ChuckJokes
//
//  Created by Jacob Reyes on 4/11/22.
//  
//

import Foundation
import UIKit

protocol ChuckJokesListViewProtocol: AnyObject {
    // PRESENTER -> VIEW
    var presenter: ChuckJokesListPresenterProtocol? { get set }
    func showChuckJokes(jokes: [ChuckJoke])
}

protocol ChuckJokesListRouterProtocol: AnyObject {
    // PRESENTER -> WIREFRAME
    static func createChuckJokesListModule() -> UIViewController
}

protocol ChuckJokesListPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var view: ChuckJokesListViewProtocol? { get set }
    var interactor: ChuckJokesListInteractorInputProtocol? { get set }
    var router: ChuckJokesListRouterProtocol? { get set }
    
    func viewDidLoad()
}

protocol ChuckJokesListInteractorOutputProtocol: AnyObject {
// INTERACTOR -> PRESENTER
    func showTenJokes(jokes: ChuckJoke?)
}

protocol ChuckJokesListInteractorInputProtocol: AnyObject {
    // PRESENTER -> INTERACTOR
    var presenter: ChuckJokesListInteractorOutputProtocol? { get set }
//    var localDatamanager: ChuckJokesListLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: ChuckJokesListRemoteDataManagerInputProtocol? { get set }
    
    func getChuckJokes()
}

protocol ChuckJokesListDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> DATAMANAGER
}

protocol ChuckJokesListRemoteDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: ChuckJokesListRemoteDataManagerOutputProtocol? { get set }
    func getChuckJokes()
}

protocol ChuckJokesListRemoteDataManagerOutputProtocol: AnyObject {
    // REMOTEDATAMANAGER -> INTERACTOR
    func setChuckJokes(with joke: ChuckJoke?)
}

protocol ChuckJokesListLocalDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> LOCALDATAMANAGER
}
