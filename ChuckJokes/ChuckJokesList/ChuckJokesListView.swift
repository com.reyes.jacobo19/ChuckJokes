//
//  ChuckJokesListView.swift
//  ChuckJokes
//
//  Created by Jacob Reyes on 4/11/22.
//  
//

import Foundation
import UIKit

class ChuckJokesListView: UIViewController {

    @IBOutlet weak var chuckListTableView: UITableView!
    // MARK: Properties
    var presenter: ChuckJokesListPresenterProtocol?
    var tenJokes: [ChuckJoke] = [ChuckJoke(createdAt: "2020-01-05 13:42:23.240175", iconURL: "https://assets.chucknorris.host/img/avatar/chuck-norris.png", id: "7s1-f6gsQmC3E4avEiKiFA", updatedAt: "2020-01-05 13:42:23.240175", url: "https://api.chucknorris.io/jokes/7s1-f6gsQmC3E4avEiKiFA", value: "Chuck Norris can beatbox at over 500 bpm.")]
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.viewDidLoad()
    }
}

extension ChuckJokesListView: ChuckJokesListViewProtocol {
    func showChuckJokes(jokes: [ChuckJoke]) {
        self.tenJokes = jokes
        DispatchQueue.main.async {
            self.chuckListTableView.reloadData()
        }
    }
}


extension ChuckJokesListView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.tenJokes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        print("el chistes \(self.tenJokes[indexPath.row])")
        let jokes = self.tenJokes[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ChuckJokeTableViewCell
        cell.setupView(chuckImage: jokes.iconURL, chuckJoke: jokes.value)
        
        return cell
    }
}


