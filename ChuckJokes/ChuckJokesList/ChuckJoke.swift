//
//  ChuckJoke.swift
//  ChuckJoke
//
//  Created by Jacob Reyes on 4/11/22.
//

import Foundation

// MARK: - ChuckJokes
struct ChuckJoke: Decodable {
    var categories: [String] =  []
    let createdAt: String
    let iconURL: String
    let id, updatedAt: String
    let url: String
    let value: String

    enum CodingKeys: String, CodingKey {
        case categories
        case createdAt = "created_at"
        case iconURL = "icon_url"
        case id
        case updatedAt = "updated_at"
        case url, value
    }
}
