//
//  ChuckJokesListInteractor.swift
//  ChuckJokes
//
//  Created by Jacob Reyes on 4/11/22.
//  
//

import Foundation

class ChuckJokesListInteractor: ChuckJokesListInteractorInputProtocol {
    

    // MARK: Properties
    weak var presenter: ChuckJokesListInteractorOutputProtocol?
//    var localDatamanager: ChuckJokesListLocalDataManagerInputProtocol?
    var remoteDatamanager: ChuckJokesListRemoteDataManagerInputProtocol?
    
    func getChuckJokes() {
        remoteDatamanager?.getChuckJokes()
    }

}

extension ChuckJokesListInteractor: ChuckJokesListRemoteDataManagerOutputProtocol {
    // TODO: Implement use case methods
    
    func setChuckJokes(with joke: ChuckJoke?) {
        
//        if self.jokes.count == 10{
            self.presenter?.showTenJokes(jokes: joke)
//        }
//        else if self.jokes.count < 10 {
//            if let joke = joke {
//                self.jokes.append(joke)
//            }
//
//            remoteDatamanager?.getChuckJokes()
//        }
        
        
    }
}
