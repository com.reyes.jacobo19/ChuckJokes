//
//  ChuckJokeTableViewCell.swift
//  ChuckJokes
//
//  Created by Jacob Reyes on 4/11/22.
//

import UIKit

class ChuckJokeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var chuckImage: UIImageView!
    @IBOutlet weak var chuckJoke: UILabel!
    
    
    func setupView( chuckImage: String, chuckJoke: String) {
        
        if let imageUrl: URL = URL(string: chuckImage) {
            self.chuckImage.load(url: imageUrl, completion: { (downloaded) in })
        }
        
        self.chuckJoke.text = chuckJoke.isEmpty ? "Label" : chuckJoke
    }
}
