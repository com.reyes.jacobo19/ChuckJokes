//
//  ChuckJokesListPresenter.swift
//  ChuckJokes
//
//  Created by Jacob Reyes on 4/11/22.
//  
//

import Foundation

class ChuckJokesListPresenter  {
    
    // MARK: Properties
    weak var view: ChuckJokesListViewProtocol?
    var interactor: ChuckJokesListInteractorInputProtocol?
    var router: ChuckJokesListRouterProtocol?
    private var jokes: [ChuckJoke] = []
    
}

extension ChuckJokesListPresenter: ChuckJokesListPresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
        interactor?.getChuckJokes()
    }
}

extension ChuckJokesListPresenter: ChuckJokesListInteractorOutputProtocol {
    // TODO: implement interactor output methods
    
    func showTenJokes(jokes: ChuckJoke?) {
        
        if self.jokes.count == 10{
            self.view?.showChuckJokes(jokes: self.jokes)
        }
        else if self.jokes.count < 10 {
            if let joke = jokes {
                self.jokes.append(joke)
            }

            interactor?.getChuckJokes()
        }
        
    }
    
}
