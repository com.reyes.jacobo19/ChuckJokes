//
//  ChuckJokesListRemoteDataManager.swift
//  ChuckJokes
//
//  Created by Jacob Reyes on 4/11/22.
//  
//

import Foundation

class ChuckJokesListRemoteDataManager:ChuckJokesListRemoteDataManagerInputProtocol {
    var remoteRequestHandler: ChuckJokesListRemoteDataManagerOutputProtocol?
 
    func getChuckJokes() {
        Connection().request() { data, response, error in
            self.responseProcess(data: data, response: response, error: error) { (chuckJoke: ChuckJoke?, error, errorCode) in
                 self.remoteRequestHandler?.setChuckJokes(with: chuckJoke)
            }
        }
    }
    
    func responseProcess<T: Decodable>(data: Data?, response: URLResponse?, error: Error?, completion: @escaping (T?, _ error: String?, _ errorcode: Int?) -> Void) {
        
        if let errorNetWork = error {
            completion(nil,errorNetWork.localizedDescription, errorNetWork._code)
        }
        
        if let d = data {
            do {
                let apiResponse = try JSONDecoder().decode(T.self, from: d)
                completion(apiResponse, nil, nil)
            } catch{
                completion(nil, error.localizedDescription, error._code)
            }
        }
        
    }
}
