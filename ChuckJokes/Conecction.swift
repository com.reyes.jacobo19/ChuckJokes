//
//  connection.swift
//  ChuckJokes
//
//  Created by Jacob Reyes on 4/11/22.
//

import Foundation


class Connection {
    
//    NetworkRouterCompletion = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void
    func request(callBack: @escaping(_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void){
        
        guard let url = URL(string: "https://api.chucknorris.io/jokes/random") else {
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            if let data = data {
                
                callBack(data, response, error)
                
            } else if let error = error {
                print("HTTP Request Failed \(error)")
            }
        }
        
        task.resume()
    }
}
